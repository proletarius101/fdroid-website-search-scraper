#!/usr/bin/env python3

import re
from glob import glob
from typing import List

import requests
import yaml

FDROID_WEBSITE_CONFIG_URL = (
    "https://gitlab.com/fdroid/fdroid-website/-/raw/master/_config.yml"
)


def generate_job(language_code: str, index_uid: str) -> str:
    return f"""scrape-{index_uid}-{language_code}:
            stage: deploy
            image: getmeili/docs-scraper:latest
            before_script:
                - find ./configs -type f -print0 | xargs -0 sed -i 's#/en/#/{language_code}/#g'
            script:
                - cd /docs-scraper
                - pipenv run ./docs_scraper $CI_PROJECT_DIR/configs/{index_uid}.json
    """


def generate_config(languages: List[str], index_uids: List[str]) -> str:
    result = [
        generate_job(language, index_uid)
        for index_uid in index_uids
        for language in languages
    ]
    return "\n".join(result)


if __name__ == "__main__":
    # generate language list from fdroid-website config
    fdroid_website_config = yaml.load(
        requests.get(FDROID_WEBSITE_CONFIG_URL).text, yaml.Loader
    )
    languages = fdroid_website_config["languages"]

    index_name_regex = re.compile("configs\/([A-Za-z0-9_]+)\.json$")
    index_uids = [
        index_name_regex.match(path).groups()[0] for path in glob("configs/*.json")
    ]

    generated_config = generate_config(languages, index_uids)

    # output file
    print(generated_config)
